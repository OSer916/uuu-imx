#include <linux/module.h>

static struct module* this_module = THIS_MODULE;
//static struct module_layout mod_core_layout = { 0 };
//static struct module_layout mod_init_layout = { 0 };
static int __init module_info_init(void)
{
    pr_info("init\n");

    pr_info("This Module Name: %s\n", this_module->name);


    print_modules();

    return 0;
}

static void __exit module_info_exit(void)
{
    pr_info("exit\n");
}


module_init(module_info_init);
module_exit(module_info_exit);

MODULE_AUTHOR("OSer");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("Linux Module Study.");
MODULE_VERSION("v1");


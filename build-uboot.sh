#!/bin/sh

DIR=$HOME/github/uboot-imx

source /opt/fsl-imx-fb/4.14-sumo/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

cd $DIR
make distclean
make mx6ull_14x14_myir_defconfig
make menuconfig
make
cd -



#! /bin/sh

UUU_DIR=$HOME/github/uuu-imx

BUSYBOX_DIR=$HOME/github/busybox-imx
ROOTFS_DIR=${BUSYBOX_DIR}/_install
ROOTFS=fsl-image-mfgtool-initramfs-imx6ull14x14_myir.cpio.gz
ROOTFS_UBOOT=${ROOTFS}.u-boot

echo $ROOTFS
echo $ROOTFS_UBOOT

rm -rf ${ROOTFS}
rm -rf ${ROOTFS_UBOOT}

cd ${ROOTFS_DIR}
find . | cpio -o -H newc | gzip -9 > ${UUU_DIR}/$ROOTFS
cd -

mkimage -n 'Ramdis Image' -A arm -O linux -T ramdisk -C gzip -d ${ROOTFS} ${ROOTFS_UBOOT}


# decompress file.gz
# gzip -d file.gz
# decompress file.cpio
# cpio -idmv < file.cpio

#! /bin/sh

UUU_DIR=$HOME/github/uuu-imx

BUSYBOX_DIR=${UUU_DIR}/busybox-imx
ROOTFS_DIR=${BUSYBOX_DIR}/_install



rm -rf rootfs.tar.bz2
rm -rf $ROOTFS_DIR

cd $BUSYBOX_DIR
make menuconfig
make 
make install
cd -


cd $ROOTFS_DIR 
mkdir -p lib
cp -d $UUU_DIR/basefiles/lib* lib/
cp -d $UUU_DIR/basefiles/ld* lib/
cp -d $UUU_DIR/basefiles/adbd usr/bin/
cp -d $UUU_DIR/basefiles/android-gadget-setup usr/bin

mkdir -p proc
mkdir -p dev
mkdir -p sys
mkdir -p tmp
mkdir -p run

mkdir -p etc

cp $UUU_DIR/basefiles/inittab etc/
cp $UUU_DIR/basefiles/fstab etc/fstab
cp $UUU_DIR/basefiles/group etc/
cp $UUU_DIR/basefiles/shadow etc/
cp $UUU_DIR/basefiles/passwd etc/
cp $UUU_DIR/basefiles/hostname etc/
cp $UUU_DIR/basefiles/hosts etc/
cp $UUU_DIR/basefiles/mdev.conf etc/

mkdir -p  etc/network
mkdir -p  etc/network/if-pre-up.d
mkdir -p  etc/network/if-up.d
mkdir -p  etc/network/if-post-down.d
mkdir -p  etc/network/if-down.d
cp $UUU_DIR/basefiles/interfaces etc/network/

mkdir -p etc/init.d
cp $UUU_DIR/basefiles/rc* etc/init.d/
cp $UUU_DIR/basefiles/S??* etc/init.d/

tar -cvjf ${UUU_DIR}/rootfs.tar.bz2 *
cd -


